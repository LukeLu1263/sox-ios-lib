##How To Install?

###For Xcode 5
	cd $soxProject
	autoreconf -i

Place the sox_ios_build_for_xcode5.sh in $soxProject and run it.

	./sox_ios_build_for_xcode5.sh
for more arguments, you can look into the script.


###For Xcode 4
	The operations are same as xcode 5 but replace the sox_ios_build_for_xcode5.sh with sox_ios_build_for_xcode4.sh.


##Add Into Your Xcode Project

You need copy the  sox.framework, sox.h to your project.
	
See the Demo program in the root directory.